#!/bin/sh

DEVICETYPE=$1
JOB_ID=$2
UI=$3

cat boot.pts | sed "s|<<DEVICE>>|$DEVICETYPE|g" | \
	       sed "s|<<JOB_ID>>|$JOB_ID|g" | \
	       sed "s|<<UI>>|$UI|g" \
	> /tmp/manifest.pts

pts-cli submit --follow --note "Boot test for $DEVICETYPE with ui $UI" /tmp/manifest.pts
