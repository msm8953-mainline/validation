#!/bin/sh -e
# SPDX-License-Identifier: GPL-3.0-or-later

DEVICE=$1
UI=$2

if [ -z "$DEVICE" ]; then
	echo "error: \$DEVICE is not set"
	exit 1
fi

if [ -z "$UI" ]; then
	echo "error: \$UI is not set"
	exit 1
fi

pmbootstrap config device "$DEVICE"
pmbootstrap config ui "$UI"

# hacks hacks hack
pmbootstrap chroot -r apk add device-$DEVICE
# Edit deviceinfo_cmdline to enable the console
sudo sed -i 's|console=null|console=ttyMSM0,115200 PMOS_NO_OUTPUT_REDIRECT|g' ~/.local/var/pmbootstrap/chroot_rootfs_"$DEVICE"/etc/deviceinfo || true
pmbootstrap install --password 147147
