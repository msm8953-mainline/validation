# postmarketOS boot validation

This repo ties GitLab CI into the [postmarketOS
PTS](https://pts.postmarketos.org) (Phone Test System) infrastructure to
automate real testing on real devices.
